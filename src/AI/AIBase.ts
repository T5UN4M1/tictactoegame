import {TicTacToeGame} from "../class/TicTacToeGame";


export abstract class AIBase {
    board: TicTacToeGame;

    constructor(board: TicTacToeGame) {
        this.board = board;
    }

    abstract getMove(): number;

    getPossibleMoves(): number[] {
        const possibleMoves = this.board.getPossibleMoves();
        if(possibleMoves.length < 1) {
            throw new Error("no possible move");
        }
        return possibleMoves;
    }
}