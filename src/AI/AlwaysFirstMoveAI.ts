import {AIBase} from "./AIBase";
import {TicTacToeGame} from "../class/TicTacToeGame";


export class AlwaysFirstMoveAI extends AIBase {

    constructor(board: TicTacToeGame) {
        super(board);
    }

    getMove():number {
        return this.getPossibleMoves()[0];
    }
}