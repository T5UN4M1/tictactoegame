import {TicTacToeGame} from "../class/TicTacToeGame";
import {AIBase} from "./AIBase";
import {Util} from "../service/Util";

export class RandomAI extends AIBase {

    constructor(board: TicTacToeGame) {
        super(board);
    }

    getMove(): number {
        const possibleMoves = this.getPossibleMoves();
        return possibleMoves[Util.rand(0, possibleMoves.length - 1)]
    }
}