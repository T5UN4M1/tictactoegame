import React from 'react';
import './App.css';
import {GameModeSelector} from "./component/GameModeSelector";

function App() {
    return (
        <div className="App">
            <GameModeSelector/>
        </div>
    );
}

export default App;
