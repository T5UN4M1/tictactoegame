import {Util} from "../service/Util";

export const WINNING_MATRIX = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],

    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],

    [0, 4, 8],
    [2, 4, 6]
];

export enum Tile {
    X, O, NONE
}

export const tileToString = (tile: Tile) => {
    switch (tile) {
        case 0:
            return 'X';
        case 1:
            return 'O';
        default :
            return ' ';
    }
}


export const FIRST_PLAYER = Tile.X;
export const SECOND_PLAYER = FIRST_PLAYER === Tile.X ? Tile.O : Tile.X;

export class TicTacToeGame {
    protected board: Tile[];
    protected status: Tile;
    protected winner: Tile;

    constructor() {
        this.board = [...Array(9).keys()].fill(Tile.NONE);
        this.status = FIRST_PLAYER;
        this.winner = Tile.NONE;
    }

    public toIndex(x: number, y: number): number {
        return y * 3 + x;
    }

    public fromIndex(n: number): number[] {
        return [~~(n / 3), n % 3];
    }

    public getPossibleMoves(): any[] {
        if (this.isOver()) {
            return [];
        }
        return [...Array(9).keys()]
            .filter(n => this.board[n] === Tile.NONE);
    }

    public get(x: number, y: number): Tile {
        return this.board[this.toIndex(x, y)];
    }

    public get2(n: number): Tile {
        return this.board[n];
    }

    protected set(x: number, y: number, tile: Tile) {
        this.set2(this.toIndex(x, y), tile);
    }

    protected set2(n: number, tile: Tile) {
        this.board[n] = tile;
    }

    public getBoardCopy(): Tile[] {
        return [...this.board];
    }

    public getCopy(): TicTacToeGame {
        const newGame = new TicTacToeGame();
        newGame.board = [...this.board];
        newGame.status = this.status;
        newGame.winner = this.winner;

        return newGame;
    }

    public isOver(): boolean {
        return this.status === Tile.NONE;
    }

    public isPlayable(x: number, y: number): boolean {
        return this.isPlayable2(this.toIndex(x, y));
    }

    public isPlayable2(n: number): boolean {
        return Util.isClamped(n, 0, 8) &&
            this.get2(n) === Tile.NONE &&
            this.status !== Tile.NONE;
    }

    public play(x: number, y: number): boolean {
        return this.play2(this.toIndex(x, y));
    }

    public play2(n: number): boolean {
        if (!this.isPlayable2(n)) {
            return false;
        }

        this.set2(n, this.status);

        this.checkFull();
        this.checkWinner();

        if (this.status !== Tile.NONE) {
            this.status = this.getNextPlayer();
        }

        return true;
    }

    protected getNextPlayer() {
        return (this.status === Tile.X) ? Tile.O : Tile.X;
    }

    public getStatus(): Tile {
        return this.status;
    }

    public getWinner(): Tile {
        return this.winner;
    }

    protected checkWinner() {
        let winner: Tile = Tile.NONE;

        WINNING_MATRIX.every((line: number[]) => {
            if (this.board[line[0]] !== Tile.NONE && this.board[line[0]] === this.board[line[1]] && this.board[line[1]] === this.board[line[2]]) {
                winner = this.board[line[0]];
                return false;
            }
            return true;
        });

        if (winner !== Tile.NONE) {
            this.winner = winner;
            this.status = Tile.NONE;
        }
    }

    protected checkFull() {
        if (this.isFull()) {
            this.status = Tile.NONE;
        }
    }

    protected isFull(): boolean {
        return this.board.every(t => t !== Tile.NONE);
    }
}
