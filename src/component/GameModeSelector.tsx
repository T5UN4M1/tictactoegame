import React, {useState} from "react";
import {TicTacToeGameDisplay} from "./TicTacToeGameDisplay";


export enum SelectorState {
    MENU,
    LOCAL_GAME2P,
    LOCAL_GAME1P
};

const Menu = (props: any) => {
    const clickHandler = props.clickHandler;

    return (
        <div>
            <ul>
                <li><a onClick={() => clickHandler(SelectorState.LOCAL_GAME2P)}>2 players on same computer</a></li>
                <li><a onClick={() => clickHandler(SelectorState.LOCAL_GAME1P)}> against AI</a></li>
            </ul>
        </div>
    )
}


export const GameModeSelector = (props: any) => {
    const [state, setState] = useState(SelectorState.MENU);

    const clickHandler = (id: SelectorState) => {
        setState(id);
    }

    switch (state) {
        case SelectorState.MENU:
            return <Menu clickHandler={clickHandler}/>;
        case SelectorState.LOCAL_GAME1P:
            return <TicTacToeGameDisplay mode={state}/>;
        default:
        case SelectorState.LOCAL_GAME2P:
            return <TicTacToeGameDisplay mode={state}/>;
    }
}