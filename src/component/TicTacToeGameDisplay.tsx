import React, {useState} from "react";
import {TicTacToeGame, Tile, tileToString} from "../class/TicTacToeGame";
import {SelectorState} from "./GameModeSelector";
import {RandomAI} from "../AI/RandomAI";


const TicTacToeGameCellDisplay = (props: { val: Tile, id: number, clickHandler: Function }) => {
    const clickHandler = () => props.clickHandler(props.id);
    return (
        <div className="tttCell" onClick={clickHandler}>{tileToString(props.val)}</div>
    )
}

export const TicTacToeGameDisplay = (props: any) => {
    const [board, setBoard] = useState(new TicTacToeGame());

    const clickHandler = (id: number) => {
        if (board.isPlayable2(id)) {
            const newBoard = board.getCopy();
            newBoard.play2(id);
            if (props.mode === SelectorState.LOCAL_GAME1P && !newBoard.isOver()) {
                const ai = new RandomAI(newBoard);
                newBoard.play2(ai.getMove());
            }
            setBoard(newBoard);
        }
    }
    return (
        <div className="tttContainer">
            {board.getBoardCopy().map((item, id) =>
                <TicTacToeGameCellDisplay val={item} id={id} key={id} clickHandler={clickHandler}/>)}
        </div>
    )
}