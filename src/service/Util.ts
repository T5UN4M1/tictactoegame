export const Util = {
        isClamped: (b: number, min: number, max: number): boolean => b <= max && b >= min,
        rand: (min: number, max: number): number => {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }
;