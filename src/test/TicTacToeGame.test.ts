import {FIRST_PLAYER, TicTacToeGame, Tile} from "../class/TicTacToeGame";


describe("Testing TicTacToe", () => {
    test("Elementary testing", () => {
        const game: TicTacToeGame = new TicTacToeGame();

        expect(game.getStatus()).toBe(FIRST_PLAYER);
        expect(game.getWinner()).toBe(Tile.NONE);

        expect(game.isPlayable(0,0)).toBe(true);
        expect(game.isPlayable(2,2)).toBe(true);
        expect(game.isPlayable(-1,0)).toBe(false);
        expect(game.isPlayable(3,0)).toBe(false);

        expect(game.get(1,1)).toBe(Tile.NONE);

        expect(game.play(0,0)).toBe(true);
        expect(game.get(0,0)).toBe(FIRST_PLAYER);

        expect(game.play(0,1)).toBe(true);
        expect(game.play(1,0)).toBe(true);
        expect(game.play(1,1)).toBe(true);
        expect(game.play(2,0)).toBe(true);

        expect(game.play(2,1)).toBe(false);
        expect(game.getStatus()).toBe(Tile.NONE);
        expect(game.getWinner()).toBe(FIRST_PLAYER);

    });
});