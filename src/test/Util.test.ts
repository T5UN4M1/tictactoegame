import {Util} from "../service/Util";

describe("Testing Util functions", () => {
    test("Testing isClamped", () => {
        const expectTrue = (n:number,min:number,max:number) => expect(Util.isClamped(n,min,max)).toBe(true);
        const expectFalse = (n:number,min:number,max:number) => expect(Util.isClamped(n,min,max)).toBe(true);

        expectTrue(3,3,3);
        expectTrue(0,0,2);
        expectTrue(2,0,2);
    });
});